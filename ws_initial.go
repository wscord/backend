package main

import (
	"sort"
	"strconv"
	"strings"

	"github.com/RumbleFrog/discordgo"
)

func fmtGroup(users []*discordgo.User) string {
	var userstr []string
	for _, u := range users {
		userstr = append(userstr, u.Username)
	}

	return strings.Join(userstr, ", ")
}

type sortCats struct {
	Channels      []*discordgo.Channel
	CategoryOrder int
	CategoryName  string
	CategoryID    int64
}

// InitialRequest handles start-up reques
func (pl Payload) InitialRequest() (*Payload, error) {
	servers := []ServerData{}
	dmChannels := []ChannelData{}

	for _, dmCh := range d.State.PrivateChannels {
		var userstr []string
		for _, u := range dmCh.Recipients {
			userstr = append(userstr, u.Username)
		}

		var custom = "dm"
		if dmCh.Type == discordgo.ChannelTypeGroupDM {
			custom += " group"
		}

		dmChannels = append(dmChannels, ChannelData{
			ID:       strconv.FormatInt(dmCh.ID, 10),
			Name:     strings.Join(userstr, ", "),
			Messages: nil,
			Custom:   custom,
		})
	}

	servers = append(servers, ServerData{
		Name:     "Direct Messages",
		Channels: dmChannels,
	})

	for _, server := range d.State.Guilds {
		channels := []ChannelData{}

		// map[ch_id][]channels
		categorizedChannels := make(map[int64]sortCats)
		categorizedChannels[0] = sortCats{
			CategoryName:  "",
			CategoryOrder: -1,
			Channels:      []*discordgo.Channel{},
		}

		for _, channel := range server.Channels {
			switch channel.Type {
			case discordgo.ChannelTypeGuildCategory:
				t := categorizedChannels[channel.ID]
				t.CategoryName = channel.Name
				t.CategoryOrder = channel.Position
				t.CategoryID = channel.ID
				categorizedChannels[channel.ID] = t
			}
		}

		for _, channel := range server.Channels {
			switch channel.Type {
			case discordgo.ChannelTypeGuildText:
				t := categorizedChannels[channel.ParentID]
				t.Channels = append(
					t.Channels,
					channel,
				)
				categorizedChannels[channel.ParentID] = t
			}
		}

		var sortedCats []sortCats
		for _, category := range categorizedChannels {
			sortedCats = append(sortedCats, category)
		}

		sort.SliceStable(sortedCats, func(i, j int) bool {
			return sortedCats[i].CategoryOrder < sortedCats[j].CategoryOrder
		})

		for _, category := range sortedCats {
			channels = append(channels, ChannelData{
				ID:       strconv.FormatInt(category.CategoryID, 10),
				Name:     category.CategoryName,
				Messages: nil,
				Custom:   "category",
			})

			sort.SliceStable(category.Channels, func(i, j int) bool {
				return category.Channels[i].Position < category.Channels[j].Position
			})

			for _, channel := range category.Channels {
				channels = append(channels, ChannelData{
					ID:       strconv.FormatInt(channel.ID, 10),
					Name:     channel.Name,
					Messages: nil,
					Custom:   "",
				})
			}
		}

		servers = append(servers, ServerData{
			Name:     server.Name,
			Channels: channels,
		})
	}

	plSend, err := craftPayload(
		Initial,
		InitialPayload{
			Servers: servers,
		},
	)

	if err != nil {
		return nil, err
	}

	return plSend, nil
}
