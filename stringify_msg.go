package main

import (
	"fmt"
	"log"
	"strings"

	"github.com/gomarkdown/markdown"

	"github.com/RumbleFrog/discordgo"
)

func parseEmoji(content string) string {
	var parsed []string

	for _, l := range strings.Split(content, " ") {
		if len(l) > 18 {
			parts := strings.Split(l, ":")
			if len(parts) < 3 {
				goto Skip
			}

			id := strings.TrimSuffix(parts[2], "\\>")

			switch {
			case strings.HasPrefix(l, "<:"):
				parsed = append(
					parsed,
					"<img class=\"emoji\" src=\"https://cdn.discordapp.com/emojis/"+id+".png\" />",
				)
			case strings.HasPrefix(l, "<a:"):
				parsed = append(
					parsed,
					"<img class=\"emoji animated\" src=\"https://cdn.discordapp.com/emojis/"+id+".gif\" />",
				)
			default:
				goto Skip
			}
		} else {
			goto Skip
		}

		continue
	Skip:
		parsed = append(parsed, l)
	}

	return strings.Join(parsed, " ")
}

// StringifyMessage takes in a message and returns a formatted one
func StringifyMessage(m *discordgo.Message) string {
	if m == nil {
		return ""
	}

	var lines []string

	content, err := m.ContentWithMoreMentionsReplaced(d)
	if err != nil {
		log.Println(err.Error())
	}

	content = parseEmoji(strings.Replace(content, ">", "\\>", -1))

	if string(m.EditedTimestamp) != "" {
		content += "<div class=\"edited\"></div>"
	}

	lines = append(lines, content)

	// for _, user := range m.Mentions {
	// 	content = strings.NewReplacer(
	// 		"<@"+strconv.FormatInt(user.ID, 10)+">", "<div class=\"mention\">@"+user.Username+"</div>",
	// 		"<@!"+strconv.FormatInt(user.ID, 10)+">", "<div class=\"mention\">@"+user.Username+"</div>",
	// 	).Replace(content)
	// }

	for _, e := range m.Embeds {
		lines = append(
			lines,
			"<div class=\"embed\"><div class=\"embed-body\">",
		)

		if e.Author != nil {
			var img string
			if e.Author.IconURL != "" {
				img = "<a href=\"" + e.Author.IconURL + "\"><img src=\"" + e.Author.ProxyIconURL + "\"/></a>"
			}

			lines = append(
				lines,
				fmt.Sprintf(
					"<div class=\"embed-author\"><a href=\"%s\">%s%s</a></div>",
					e.Author.URL,
					img,
					e.Author.Name,
				),
			)
		}

		if e.Title != "" {
			lines = append(
				lines,
				fmt.Sprintf("<div class=\"embed-title\">%s</div>", e.Title),
			)
		}

		if e.Description != "" {
			lines = append(
				lines,
				fmt.Sprintf(
					"<div class=\"embed-description\">%s</div>",
					parseEmoji(strings.Replace(e.Description, ">", "\\>", -1)),
				),
			)
		}

		if len(e.Fields) > 1 {
			for _, f := range e.Fields {
				lines = append(
					lines,
					fmt.Sprintf(
						"<div class=\"fields inline-%v\"><div class=\"field-n\">%s</div><div class=\"field-v\">%s</div></div>",
						f.Inline,
						f.Name,
						f.Value,
					),
				)
			}
		}

		if e.Footer != nil {
			lines = append(
				lines,
				fmt.Sprintf(
					"<div class=\"embed-footer\"><a href=\"%s\"><img src=\"%s\"/></a>%s</div>",
					e.Footer.IconURL,
					e.Footer.ProxyIconURL,
					e.Footer.Text,
				),
			)
		}

		lines = append(
			lines,
			"</div>",
		)

		if e.Thumbnail != nil {
			lines = append(
				lines,
				fmt.Sprintf(
					"<div class=\"embed-thumbnail\"><a href=\"%s\"><img src=\"%s\" /></a></div>",
					e.Thumbnail.URL,
					e.Thumbnail.ProxyURL,
				),
			)
		}

		lines = append(
			lines,
			"</div>",
		)
	}

	for _, i := range m.Attachments {
		switch i.Height {
		case 0:
			lines = append(
				lines,
				fmt.Sprintf(
					"<div class=\"attachment\"><a href=\"%s\">%s</a></div>",
					i.URL,
					i.Filename,
				),
			)
		default:
			lines = append(
				lines,
				fmt.Sprintf(
					"<div class=\"attachment\"><a href=\"%s\"><img src=\"%s\" alt=\"%s\"/></a></div>",
					i.ProxyURL,
					i.URL,
					i.Filename,
				),
			)
		}

	}

	md := markdown.ToHTML(
		[]byte(strings.Join(lines, "\n")),
		nil, nil,
	)

	return string(md)
}
