package main

import (
	"encoding/json"
	"errors"
	"log"
	"net/http"
	"time"

	"github.com/satori/go.uuid"

	"github.com/gorilla/websocket"
)

var (
	upgrader = websocket.Upgrader{
		EnableCompression: true,
	}

	// ErrNilPtr is returned when variable is a nil pointer
	ErrNilPtr = errors.New("Nil pointer")

	// ErrSessionExpired is returned when the client's session is expired
	ErrSessionExpired = errors.New("Session expired")
)

// Serve starts the webserver at address
func Serve(address string) {
	http.HandleFunc("/api/ws", Websocket)

	go StartSocket()

	log.Println("Serving at", address)

	log.Println(
		http.ListenAndServe(address, nil),
	)
}

// Websocket handles and upgrades incoming connections
func Websocket(w http.ResponseWriter, r *http.Request) {
	upgrader.CheckOrigin = func(r *http.Request) bool { return true }

	if len(cfg.Challenges) > 0 {
		key, ok := r.Header["Sec-WebSocket-Protocol"]
		if !ok || len(key) != len(cfg.Challenges) {
			w.WriteHeader(403)
			pl, _ := craftPayload(Challenge, ChallengePayload{
				Count: len(cfg.Challenges),
			})

			j, _ := json.Marshal(pl)

			w.Write(j)
			return
		}

		for i := 0; i < len(cfg.Challenges); i++ {
			if key[i] != cfg.Challenges[i] {
				w.WriteHeader(403)
				w.Write([]byte("Unauthorized: incorrect challenge"))
				return
			}
		}
	}

	ws, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Println("ERROR! Upgrader failed!", err.Error())
	}

	// Closes when function returns
	defer ws.Close()

	if _, ok := clients[ws]; !ok {
		u, err := uuid.NewV4()
		if err != nil {
			log.Println(err.Error())
			return
		}

		clients[ws] = Session{
			UUID:    u.String(),
			Channel: 0,
			Expire:  time.Now().Add(cfg.expiry),
		}
	}

	// Loop stuff to continuously listen
	for {
		pl := Payload{}

		if err := ws.ReadJSON(&pl); err != nil {
			log.Println("Closing:", err.Error())
			delete(clients, ws)
			break
		}

		// Sends the "pl" (Payload) to "broadcast"
		broadcast <- pl
	}
}

// Router switches the payload type
func (pl *Payload) Router(conn *websocket.Conn, session Session) (*Payload, error) {
	switch pl.Type {
	case Initial:
		return pl.InitialRequest()
	case RequestMessages:
		req := &RequestMessagesPayload{}

		if err := json.Unmarshal(pl.Data, req); err != nil {
			return nil, err
		}

		if req == nil {
			return nil, ErrNilPtr
		}

		return pl.FetchMessages(conn, req)
	case Post:
		req := &PostPayload{}

		if err := json.Unmarshal(pl.Data, req); err != nil {
			return nil, err
		}

		if req == nil {
			return pl.PostMessage(conn, req)
		}
	}

	return nil, nil
}
