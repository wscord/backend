package main

import (
	"encoding/json"
	"errors"
	"log"
	"net/http"
	"sync"
	"time"

	"github.com/gorilla/websocket"
)

var (
	clients = make(map[*websocket.Conn]Session)

	broadcast = make(chan Payload)

	wsMutex = sync.Mutex{}

	// ErrUnknown is returned when the error is uncommon, so it's logged into console
	ErrUnknown = errors.New("Unknown error, check console")

	// ErrUnknownPayload is a convenient payload
	ErrUnknownPayload, _ = craftPayload(
		Error,
		ErrUnknown,
	)
)

// Session is the information for each websocket session
type Session struct {
	UUID      string
	Channel   int64
	UnreadChs []int64
	Expire    time.Time
}

// PayloadType is the type of payload the ws is sending
type PayloadType string

const (
	// Error is used for type when the payload is an error message
	Error PayloadType = "error"

	// Challenge is used when the backend requires authentication
	Challenge PayloadType = "challenge"

	// Initial .
	Initial PayloadType = "initial"

	// RequestMessages is used when the client wants messages from a channel
	RequestMessages PayloadType = "req_msg"

	// BulkMessages is used when the server responds with a lot of messages in a channel
	BulkMessages PayloadType = "bulk_msg"

	// Post is used for type when the client sends a message
	Post PayloadType = "post"

	// Edit is used for type when a message is edited
	Edit PayloadType = "edit"

	// Delete is used for type when a message is deleted
	Delete PayloadType = "delete"

	// ReactionAdd is used for type when a reaction is added
	ReactionAdd PayloadType = "reaction_add"

	// ReactionEdit is used for type when a reaction is edited
	ReactionEdit PayloadType = "reaction_edit"

	// ReactionDelete is used for type when a reaction is deleted
	ReactionDelete PayloadType = "reaction_delete"
)

// Payload is the payload a server will send
type Payload struct {
	Type PayloadType     `json:"type"`
	Data json.RawMessage `json:"data"`
}

// ErrorPayload is used for Data when the payload is an error
type ErrorPayload string

/*
	REQUEST PAYLOADS
*/

// RequestMessagesPayload should be received when the Type is RequestMessages, requested from the clients
type RequestMessagesPayload struct {
	ChannelID int64 `json:"channel_id,string"`
}

// PostPayload should be received when the Type is Post, requested from the clients
type PostPayload struct {
	Content string `json:"content"`
}

/*
	RESPONSE PAYLOADS
*/

type ChallengePayload struct {
	Count int `json:"count"`
}

// InitialPayload is returned when requested
type InitialPayload struct {
	Servers []ServerData `json:"servers"`
}

// BulkMessagesPayload is used when the response is a bunch of messages (for initial and request)
type BulkMessagesPayload struct {
	Messages map[int64]MessageData `json:"messages"`
	PM       bool                  `json:"pm"`
}

// ServerData contains the data for one server (or guild)
type ServerData struct {
	Name     string        `json:"name"`
	Channels []ChannelData `json:"channels"`
}

// ChannelData contains the data for one channel
type ChannelData struct {
	ID       string                `json:"id"`
	Name     string                `json:"name"`
	Messages map[int64]MessageData `json:"messages"`
	Custom   string                `json:"c"`
}

// MessageData contains the data for a message
type MessageData struct {
	Author  string `json:"author"`
	Color   string `json:"color"`
	Content string `json:"content"` // markdown to html
}

// StartSocket starts handler for socket
func StartSocket() {
	for {
		// Listen to broadcast for rq (*Request)
		rq := <-broadcast

		// Broadcast to all clients
		for client, session := range clients {
			if session.Expire.Sub(time.Now()) < 0 {
				sockError(client, ErrSessionExpired)
				continue
			}

			session.Expire = time.Now().Add(cfg.expiry)

			pl, err := rq.Router(client, session)
			if err != nil {
				sockError(client, err)
				continue
			}

			if pl == nil {
				continue
			}

			if err := mutWrite(client, &pl); err != nil {
				log.Println(err.Error())
			}
		}
	}
}

func mutWrite(cli *websocket.Conn, dat interface{}) error {
	wsMutex.Lock()
	defer wsMutex.Unlock()

	if err := cli.WriteJSON(dat); err != nil {
		return err
	}

	return nil
}

func sockError(cli *websocket.Conn, e error) {
	pl, err := craftPayload(
		Error,
		e.Error(),
	)

	if err != nil {
		log.Println(err.Error())
		goto Error
	}

	if err := mutWrite(cli, &pl); err != nil {
		log.Println(err.Error())
		goto Error
	}

	return

Error:
	if err := mutWrite(cli, ErrUnknownPayload); err != nil {
		log.Println(err.Error())
	}
}

// For use with HTTP
func craftError(w http.ResponseWriter, e string) {
	w.WriteHeader(500)
	pl, err := craftPayload(Error, e)
	if err == nil {
		errjson, err := json.Marshal(pl)
		if err == nil {
			w.Write(errjson)
		} else {
			goto Fallback
		}
	} else {
		goto Fallback
	}

	return
Fallback:
	w.Write([]byte(e))
}

func craftPayload(t PayloadType, data interface{}) (*Payload, error) {
	json, err := json.Marshal(data)
	if err != nil {
		return nil, err
	}

	return &Payload{
		Type: t,
		Data: json,
	}, nil
}

// func craftErrorSocket(cl *websocket.Conn, e string) {
// 	log.Println(e)
// }
