# wscord-backend

A simple Discord router using Go + Vue

## Screenshot

![](https://ptpb.pw/IhYL.png)

## Features

- Construct an embed (WIP)
- Full Rich Embed support (some CSS are broken, most are working)
- Live message polling (websocket)
- Proper measures to reduce bandwidth
- Direct Messaging, categories, etc

## Todo

- [ ] Members list
- [ ] Role colors
- [ ] Nickname
- [ ] Delete/edit messages (edit will be `:s/asd/asd` + regexp)
- [ ] Enter to send messages
- [ ] Maybe make the layout more responsive idk (it's pretty tailored for mobile atm)
- [ ] Better desktop layout
- [ ] Unread messages indicator (already WIP)