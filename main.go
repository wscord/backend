package main

import (
	"log"
	"os"
	"os/signal"
	"syscall"

	"github.com/RumbleFrog/discordgo"

	"github.com/syndtr/goleveldb/leveldb"
)

var (
	db, _ = leveldb.OpenFile("./database", nil)
	cfg   = initCfg()
	d     = initiate()

	configLoc = "config.json"

	maxMsgCount = 20
)

func initiate() *discordgo.Session {
	login, err := cfg.Discord.Initiate()
	if err != nil {
		log.Panicln(err)
	}

	d, err := discordgo.New(login.Token)
	if err != nil {
		log.Panicln(err)
	}

	return d
}

func main() {
	d.AddHandler(onReady)
	d.AddHandler(CreateMsg)
	d.AddHandler(EditMsg)

	if err := d.Open(); err != nil {
		log.Panicln(err)
	}

	Serve(":8080")

	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)
	<-sc

	defer func() {
		d.Close()
	}()
}
