package main

import (
	"errors"
	"fmt"
	"log"

	"github.com/RumbleFrog/discordgo"
)

var (
	// ErrChannelNotInitiated is returned when the client hasn't selected a channel yet
	ErrChannelNotInitiated = errors.New("Channel is not initiated")
)

func onReady(s *discordgo.Session, r *discordgo.Ready) {
	if r == nil {
		log.Println("Ready is nil")
	}

	log.Println("Ready.")
}

// CreateMsg ..
func CreateMsg(s *discordgo.Session, m *discordgo.MessageCreate) {
	var pm = false

	if m.GuildID == 0 {
		pm = true
	}

	// member := s.GuildMember(m.GuildID)

	for client, session := range clients {
		if session.Channel == 0 {
			return
		}

		if session.Channel != m.ChannelID {
			session.UnreadChs = append(session.UnreadChs, m.ChannelID)
			return
		}

		var author string
		mem, err := d.State.Member(m.GuildID, m.Author.ID)
		if err != nil || mem.Nick == "" {
			author = m.Author.String()
		} else {
			author = mem.Nick
		}

		message := make(map[int64]MessageData)
		message[m.ID] = MessageData{
			Author:  author,
			Color:   fmt.Sprintf("#%X", d.State.UserColor(m.Author.ID, m.ChannelID)),
			Content: StringifyMessage(m.Message),
		}

		pl, err := craftPayload(BulkMessages, BulkMessagesPayload{
			Messages: message,
			PM:       pm,
		})

		if err != nil {
			sockError(
				client,
				err,
			)

			return
		}

		if err := mutWrite(client, &pl); err != nil {
			log.Println(err.Error())
		}
	}
}

// EditMsg ..
func EditMsg(s *discordgo.Session, m *discordgo.MessageUpdate) {
	var pm = false

	if m.GuildID == 0 {
		pm = true
	}

	for client, session := range clients {
		if session.Channel == 0 {
			return
		}

		if session.Channel != m.ChannelID {
			return
		}

		message := make(map[int64]MessageData)
		message[m.ID] = MessageData{
			Content: StringifyMessage(m.Message),
		}

		pl, err := craftPayload(Edit, BulkMessagesPayload{
			Messages: message,
			PM:       pm,
		})

		if err != nil {
			sockError(
				client,
				err,
			)

			return
		}

		if err := mutWrite(client, &pl); err != nil {
			log.Println(err.Error())
		}
	}
}
