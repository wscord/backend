package main

import (
	"encoding/json"
	"flag"
	"io/ioutil"
	"log"
	"os"
	"time"
)

type config struct {
	Discord    loginArgs `json:"discord"`
	Token      string    `json:"token_if_not_login"`
	Challenges []string  `json:"challenges"`
	IDexpiry   int       `json:"id_expiry"`
	expiry     time.Duration
}

func initCfg() *config {
	flag.StringVar(&configLoc, "c", "config.json", "Location of config file")
	file, err := os.OpenFile(configLoc, os.O_CREATE|os.O_RDWR, os.ModePerm)
	if err != nil {
		log.Panicln(err)
	}

	content, err := ioutil.ReadAll(file)
	if err != nil {
		log.Panicln(err)
	}

	cfg := &config{}
	if err := json.Unmarshal(content, cfg); err != nil {
		c, _ := json.MarshalIndent(cfg, "", "\t")
		log.Println("\n" + string(c))
		log.Panicln(err)
	}

	cfg.expiry = time.Duration(cfg.IDexpiry * int(time.Hour))

	return cfg
}
