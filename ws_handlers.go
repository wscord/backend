package main

import (
	"encoding/csv"
	"errors"
	"fmt"
	"log"
	"strings"

	"github.com/RumbleFrog/discordgo"

	"github.com/gorilla/websocket"
)

var (
	// ErrWrongDataType is returned when type assertion fails
	ErrWrongDataType = errors.New("Wrong data type")
)

// FetchMessages fetches messages from a guild
func (pl Payload) FetchMessages(conn *websocket.Conn, req *RequestMessagesPayload) (*Payload, error) {
	session, ok := clients[conn]
	if !ok {
		return nil, ErrNilPtr
	}

	session.Channel = req.ChannelID

	clients[conn] = session

	msgs, err := d.ChannelMessages(req.ChannelID, maxMsgCount, 0, 0, 0)
	if err != nil {
		log.Println(req.ChannelID)
		return nil, err
	}

	messages := make(map[int64]MessageData)

	for _, m := range msgs {
		if m != nil {
			var author string
			mem, err := d.State.Member(m.GuildID, m.Author.ID)
			if err != nil || mem.Nick == "" {
				author = m.Author.String()
			} else {
				author = mem.Nick
			}

			messages[m.ID] = MessageData{
				Author:  author,
				Color:   fmt.Sprintf("#%X", d.State.UserColor(m.Author.ID, m.ChannelID)),
				Content: StringifyMessage(m), // todo: parse embed (done)
			}
		}
	}

	plres, err := craftPayload(BulkMessages, BulkMessagesPayload{
		Messages: messages,
	})

	if err != nil {
		return nil, err
	}

	return plres, nil
}

// PostMessage handles message sending
func (pl Payload) PostMessage(conn *websocket.Conn, req *PostPayload) (*Payload, error) {
	session, ok := clients[conn]
	if !ok {
		return nil, ErrNilPtr
	}

	if strings.HasPrefix(req.Content, "/embed ") {
		input := csv.NewReader(strings.NewReader(req.Content))
		input.Comma = ' ' // delimiter
		args, err := input.Read()
		if err != nil {
			return nil, err
		}

		var embedContent = struct {
			Title       string
			Author      string
			AuthorURL   string
			AuthorImage string
			Footer      string
			Thumbnail   string
			Content     []string
		}{}

		for i := 1; i < len(args); i++ {
			switch args[i] {
			case "-t", "--title":
				embedContent.Title = args[i+1]
				i++
			case "-a", "--author":
				embedContent.Author = args[i+1]
				i++
			case "--authorURL":
				embedContent.AuthorURL = args[i+1]
				i++
			case "--authorImage":
				embedContent.AuthorImage = args[i+1]
				i++
			case "-f", "--footer":
				embedContent.Footer = args[i+1]
				i++
			case "-th", "--thumbnail":
				embedContent.Thumbnail = args[i+1]
				i++
			default:
				embedContent.Content = append(embedContent.Content, args[i])
			}
		}

		if _, err := d.ChannelMessageSendEmbed(session.Channel, &discordgo.MessageEmbed{
			Title: embedContent.Title,
			Author: &discordgo.MessageEmbedAuthor{
				Name:    embedContent.Author,
				URL:     embedContent.AuthorURL,
				IconURL: embedContent.AuthorImage,
			},
			Footer: &discordgo.MessageEmbedFooter{
				Text: embedContent.Footer,
			},
			Description: strings.Join(embedContent.Content, " "),
			Thumbnail: &discordgo.MessageEmbedThumbnail{
				URL: embedContent.Thumbnail,
			},
		}); err != nil {
			log.Println(session.Channel)
			return nil, err
		}
	} else {
		if _, err := d.ChannelMessageSend(session.Channel, req.Content); err != nil {
			return nil, err
		}
	}

	return nil, nil
}
