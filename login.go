package main

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
)

// Login contains the received token
type Login struct {
	Token string `json:"token"`
}

// Authentication is the data we send to Discord's endpoint
type Authentication struct {
	Email         string      `json:"email"`
	Password      string      `json:"password"`
	Undelete      bool        `json:"undelete"`
	CAPTCHAKey    interface{} `json:"captcha_key"`
	LoginSource   interface{} `json:"login_source"`
	GiftCodeSkuID interface{} `json:"gift_code_sku_id"`
}

type loginArgs struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

// Initiate starts an authentication and writes to a database
func (la *loginArgs) Initiate() (*Login, error) {
	if cfg.Token != "" {
		return &Login{
			Token: cfg.Token,
		}, nil
	}

	data, err := db.Get([]byte("token"), nil)
	if err == nil {
		token := string(data)
		return &Login{
			Token: token,
		}, nil
	}

	auth := &Authentication{
		Email:         la.Email,
		Password:      la.Password,
		Undelete:      false,
		CAPTCHAKey:    nil,
		LoginSource:   nil,
		GiftCodeSkuID: nil,
	}

	authjson, err := json.Marshal(auth)
	if err != nil {
		return nil, err
	}

	log.Println(string(authjson))

	client := http.Client{}

	req, err := http.NewRequest(
		"POST",
		"https://discordapp.com/api/v6/auth/login",
		bytes.NewBuffer(authjson),
	)

	if err != nil {
		return nil, err
	}

	batchHeader(map[string]string{
		"Origin":       "https://discordapp.com",
		"User-Agent":   "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3534.4 Safari/537.36",
		"Content-Type": "application/json",
		"Referer":      "https://discordapp.com/login?redirect_to=%2Fchannels%2F%40me",
		"Authority":    "discordapp.com",
		"DNT":          "1",
	}, &req.Header)

	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	resp.Body.Close()

	log.Println(string(body))

	var login Login
	if err := json.Unmarshal(body, &login); err != nil {
		return nil, err
	}

	if err := db.Put([]byte("token"), []byte(login.Token), nil); err != nil {
		return nil, err
	}

	return &login, nil
}

func batchHeader(headers map[string]string, httpheader *http.Header) {
	for k, v := range headers {
		httpheader.Add(k, v)
	}
}
