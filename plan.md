# Just some plans

1. Implement a ping type over websocket for channels with messages sent

Server->Client ping

```json
{
    "type": "ping",
    "data": {
        "channelID": [
            6xx...,
            13342...
        ]
    }
}
```

2. Server will take note that a channel message request = viewed all new messages thus:
    1. Not sending them a ping if another channel message request isn't made (to a different channel)
    2. Only start sending once the client requests on a new channel
    
3. The server will send an initial load containing all guilds and channels

```json
{
    "servers": [
        109432004: { // the idea is to send as little as possible, so only the names and the IDs
            name: "r/unixporn",
            channels: [
                230982392384204: {
                    "name": "r/unixporn",
                    ...
                },
                423849238529580: {
                    "name": "general",
                    ...
                }
            ]
        },
        88322958: {}
    ]
}
```

4. Roles should be ignored, or maybe embedded into the websocket content as color codes instead of role names and stuff

5. Reactions should be a very simple payload:

```json
{
    "type": "reactionadd",
    "data": {
        "message": 131198395025
    }
}
```

6. A typical message payload should contain: `<color>author#discrim</color>`, content, message ID and target channel ID

7. DMs may be handled as just another server for minimalism

8. Put the message ID into the divs with a context ID, so you could just use an if statement and see if the message is there or not (create if not, edit/delete if does)'

9. When sending a new message, immediately append it on the site and assume it's there unless received an error sig from the server pointing to that message

10. Layout for client->server requests

```json
{
    "type": "req_msg",
    "data": {
        "channel_id": 9423482394820
    }
}
```

# Todo

1. Implement session ID
2. Implement a map to manage sessions `map[session_ID]channel_ID` basically says the location of the client
3. Drop _all_ messages except for the one the session's in
4. Since `conn` doesn't offer any session management, the browser will use a session ID from its cookies, and if it's expired, request a new one
5. Websocket should check for cookies (session_ID), if non-existent, redirect to / where it will initiate a new UUID for the cookie

# Steps to initiate a connection

1. Client calls to websocket server, server then stores a Unique ID for that client
2. Client, in the meantime, asynchronously calls the initial endpoint for a list of guilds and channels
3. The client then sets everything up, and links the channels to appropriate websocket calls
4. On click, the client will send a payload over websocket asking for 10 messages of that channel
5. The server would then register that channel ID into the structure of that connection, then delivers the messages
6. All incoming messages are now blocked unless it's from that channel ID
7. On a channel switch, the channel ID would be changed, thus blocking other messages except the one in that channel
8. A session is dropped when a websocket is closed
   
# Authentication

- Initial should be moved over to websocket
- Initiating websocket requires the client to solve a challenge
- The challenge is served over a JSON body like so:

```json
{
	"challenge": ["field1", "field2", "field3"],
	"otp": false
}
```

- Response

```json
{
	"challenge": {
		"field1": "asdasdasd",
		"field2": "asdasdasdasd",
		"field3": "asdasdasd"
	},
	"otp": null // or an OTP code
}
```

- Challenge pairing, OTP checking and timeout will be handled on client